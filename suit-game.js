class SuitGame{
    vs = 'VS';
    result ;
    logo = './assets/logo.png';
    background = '#F9B23D';
    consoleInfo;
    player1;
    player2;
    theGame = document.querySelector(".result");

    constructor(player1, player2){
        this.player1 = player1;
        this.player2 = player2;

        this.determineTheWinner();
    }

    back(){}

    determineTheWinner() {
        if(this.player1.choice === this.player2.choice){
            this.result = this.theGame.innerHTML = `
            <div class="center-text result-box" id="draw">
              <p class="result-text">Draw</p>
            </div>`;
            this.consoleInfo = "DRAW";
            return;
        }
        const player12Choice = this.player1.choice + this.player2.choice;
        switch(player12Choice){
            case "SCISSORPAPER":
            case "PAPERROCK":
            case "ROCKSCISSOR": {
                this.result = this.theGame.innerHTML = `
                <div class="center-text result-box win" id="player-win">
                  <p class="result-text">Player 1 Win</p>
                </div>`;
                this.consoleInfo = `${this.player1.name} Win` ;
                return;
            }
            case "PAPERSCISSOR":
            case "ROCKPAPER":
            case "SCISSORROCK": {
                this.result = this.theGame.innerHTML = `
        <div class="info-box result-box win" id="comp-win">
          <p class="result-text">COM Win</p>
        </div>`;
                this.consoleInfo = `${this.player2.name} Win` ;
                return;
            }

        }
    }
}